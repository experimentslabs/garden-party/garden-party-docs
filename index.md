---
modifiable: false
has_toc: true
layout: default
navigation: 1
i18n_id: home
locale: fr
---

![Logo](assets/logo.svg){: .logo}

{: .text-center.fs-6.fw-300 }
**Garden Party** est une application OpenSource destinée à aider les jardiniers en
herbe dans la gestion de leur jardin.

[Lancez votre instance !](https://gitlab.com/experimentslabs/garden-party/garden-party/-/blob/master/README.md){: .btn.btn-green.fs-5.my-5.d-block.text-center }

## Liens

- [Sources](https://gitlab.com/experimentslabs/garden-party/garden-party)
- [Documentation technique](https://doc.garden-party.io) (_en anglais_)
- [Sources de données](https://gitlab.com/experimentslabs/garden-party/data)

Autres:

- [Sources de ce site](https://gitlab.com/experimentslabs/garden-party/garden-party-docs)

## Instances

{: .fs-3}
[Ouvrez une issue](https://gitlab.com/experimentslabs/garden-party/garden-party-docs/-/issues/new?issue[title]=New%20instance!&issue[description]=Name%3A%0AURL%3A%0AShort%20description%3A) pour partager votre instance si vous le souhaitez.

### France

- [Instance sarthoise](https://garden-party.experimentslabs.com)

## Discussions

Si vous souhaitez être au courant des mises à jour ou simplement discuter des problèmes existants et des améliorations possibles, vous pouvez :

- [rejoindre les salons Matrix](https://matrix.to/#/#garden-party:matrix.org) (chat),
- [rejoindre la plateforme de décision](https://framavox.org/garden-party/) pour proposer et discuter des nouvelles fonctionnalités
  (vous aurez besoin de créer un compte sur la plateforme avant de pouvoir rejoindre le groupe),
- ou nous contacter directement en envoyant un courriel à {% contact_email_link %}

## Fonctionnalités

{{ 'Dessinez votre jardin' | thumb_section: 'map.png', 'Placez des éléments, dessinez des parcelles et des chemins.', 'Utilisez des calques pour vous organiser.'}}

{{ 'Gérez vos éléments' | thumb_section: 'overview.png', 'Les éléments retirés sont toujours accessibles pour référence.' }}

{{ 'Planifiez vos tâches' | thumb_section: 'tasks.png', 'Créez des tâches pour tout ce que vous avez ajouté dans votre jardin.' }}

{{ 'Suivez vos récoltes' | thumb_section: 'harvests.png', 'Gardez une trace de ce que vous avez récolté et comparez avec les annnées précédentes.' }}

{{ 'Coordonez les plants' | thumb_section: 'coordination.png', 'Determinez ce que vous plantez, où et quand' }}

{{ 'Prenez des notes et des photos' | thumb_section: 'observations.png,timeline.png', 'Vous pouvez prendre des notes et des photos de tout ce qui est dans votre jardin.' }}

{{ 'Travaillez ensemble' | thumb_section: 'team.png', 'Invitez vos proches à dans votre jardin, assignez vous des tâches !' }}

{{ 'Gardez une trace de ce qui est fait' | thumb_section: 'activity.png', 'La plupart des actions réalisées sont consignées dans un journal de bord.' }}

{{ 'Faites grandir votre communauté' | thumb_section: 'library.png', "La librairie d'éléments est partagée entre les membres d'une même instance." }}

{{ 'Partagez vos cartes' | thumb_section: 'share.png', 'Rendez vos cartes consultable par tout le monde' }}
