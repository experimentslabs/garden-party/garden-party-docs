---
modifiable: false
layout: default
navigation: 1
i18n_id: home
locale: en
---

![Logo](assets/logo.svg){: .logo}

{: .text-center.fs-6.fw-300 }
**Garden Party** is an OpenSource application to help gardening newbies to manage their garden.

[Setup your instance!](https://gitlab.com/experimentslabs/garden-party/garden-party/-/blob/master/README.md){: .btn.btn-green.fs-5.my-5.d-block.text-center }

## Links

- [Sources of the project](https://gitlab.com/experimentslabs/garden-party)
- [Technical documentation](https://doc.garden-party.io)
- [Data sources](https://gitlab.com/experimentslabs/data)

Other:

- [Sources for this site](https://gitlab.com/experimentslabs/garden-party-docs)

## Instances

{: .fs-3}
[Open an issue](https://gitlab.com/experimentslabs/garden-party/garden-party-docs/-/issues/new?issue[title]=New%20instance!&issue[description]=Name%3A%0AURL%3A%0AShort%20description%3A) to share you instance!

### France

- [Sarthe](https://garden-party.experimentslabs.com)

## Discussions

If you want to be aware of updates or just chat about existing issues and
feature requests, you can...

- [join the Matrix chatrooms](https://matrix.to/#/#garden-party:matrix.org),
- [join the decision platform](https://framavox.org/garden-party/) to propose and discuss new features
  (you will need an account on the platform before being able to access the group),
- directly contact the staff by sending an email to {% contact_email_link %}

We're mostly french people but we speak english too :)

## Features

{{ 'Draw your garden' | thumb_section: 'map.png', 'Place elements, draw patches and objects', 'Use layer to organize the map'}}

{{ 'Manage your content' | thumb_section: 'overview.png', 'Removed elements are still visible in the overview' }}

{{ 'Plan with tasks' | thumb_section: 'tasks.png', 'Create tasks for everything placed in the garden' }}

{{ 'Track the harvests' | thumb_section: 'harvests.png', 'Keep track of harvested quantities and compare with previous years' }}

{{ 'Coordinate plantations' | thumb_section: 'coordination.png', 'Determine what you want to plant, where and when' }}

{{ 'Take notes and pictures' | thumb_section: 'observations.png,timeline.png', 'For everything placed in the garden, you can take notes and attach pictures' }}

{{ 'Work together' | thumb_section: 'team.png', 'Invite people on your map, assign each other tasks and get things done' }}

{{ 'Make your community grow' | thumb_section: 'library.png', 'Have a shared library of content' }}

{{ 'Keep track of your work' | thumb_section: 'activity.png', 'Most of the actions are logged in an activity journal' }}
