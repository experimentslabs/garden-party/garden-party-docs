# Licenses

## Documentation and logo

All documentation texts are under a
"[Creative Commons by](http://creativecommons.org/licenses/by/4.0/)"
license.

Logo (`assets/logo.svg`) is under a
"[Creative Commons by](http://creativecommons.org/licenses/by/4.0/)"
license. Original author is [@mtancoigne](https://gitlab.com/mtancoigne).

## Screenshots

All screenshots are in public domain

## Layout files

The following layouts are derived from the
[original theme](https://github.com/pmarsceill/just-the-docs), and are
under the
[MIT license](https://github.com/pmarsceill/just-the-docs/blob/master/LICENSE.txt):

- `_layouts/default.html`

Other template files are under the MIT license too (`_plugins/*`,
`_includes/*`) unless specified.
