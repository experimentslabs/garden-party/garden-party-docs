---
modifiable: false
has_toc: true
layout: default
nav_order: 3
i18n_id: audit
title: Audit
---

Un audit de code a été réalisé sur la version 0.13.3 par [David Dérus](https://davidderus.fr), [disponible en PDF](/assets/2023-11-24-audit-garden-party.pdf)
<small>(MD5: <code>1f6388f976454e37119d6bee268d249a</code>)</small>.

Cette page liste tous les points d'améliorations remontés ainsi que leur status.

<div style="display:flex;gap: 1em;">
  <span style="flex:none">Avancement :</span>
  <progress value="{{ site.data.audit_points | where_exp: "item", "item.done == true or item.abandoned == true" | size }}" max="{{ site.data.audit_points | size }}" style="flex-grow:1;">{{ site.data.audit_points | where_exp: "item", "item.done == true or item.abandoned == true" | size }}/{{ site.data.audit_points | size }}</progress>
  <span style="flex:none">{{ site.data.audit_points | where_exp: "item", "item.done == true or item.abandoned == true" | size }}/{{ site.data.audit_points | size }}</span>
</div>

| Section | Liens |
|---------|-------|
{%- for point in site.data.audit_points %}
| {% if point.done or point.abandoned %}~~{% endif %}**{{point.section}}** - {{ point.title }}{% if point.done or point.abandoned  %}~~{% endif %}{% if point.abandoned %} **_Abandonné_**{% endif %} | {%if point.issue_url %}[Ticket]({{point.issue_url}}){% endif %}{% if point.issue_url and point.merge_request_url %} - {% endif %}{% if point.merge_request_url %}[MR]({{point.merge_request_url}}){% endif %} |
{%- endfor %}
