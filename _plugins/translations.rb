require 'byebug'

module Translations
  PAGE_EXTENSIONS = ['.html', '.htm', '.markdown', '.md', '.mdown', '.erb'].freeze

  # Returns a div with links for translated pages
  def translation_links(article)
    other_locales = @context['site']['locales'].keys.reject { |l| l == article['locale'] }
    links         = []

    other_locales.each do |locale|
      unless article['i18n_links'] && article['i18n_links'][locale]
        links << <<~HTML
          <div class="nav-list-item">
            <a href="#" class="nav-list-link translation_missing">#{@context['site']['locales'][locale]}</a>
          </div>
        HTML
        next
      end

      links << <<~HTML
        <li class="nav-list-item">
          <a class="nav-list-link" href="#{article['i18n_links'][locale].url}">
            #{@context['site']['locales'][locale]}
          </a>
        </li>
      HTML
    end

    <<~HTML
      <div class="nav-category">#{t('layout.translations')}</div>
      <ul class="nav-list">#{links.join}</ul>
    HTML
  end

  def t(dot_path, *values)
    locale = @context['page']['locale'] || @context['page']['collection'].split('_').last
    string = @context['site']['translations'][locale].dig(*dot_path.split('.'))
    return "<span class='translation_missing'>Missing translation for #{dot_path}</span>" unless string

    string % values
  end

  def d(page_date, dot_path)
    locale = @context['page']['locale'] || @context['page']['collection'].split('_').last
    string = @context['site']['translations'][locale].dig(*dot_path.split('.'))
    return "<span class='translation_missing'>Missing date translation for #{dot_path}</span>" unless string

    date page_date, string
  end

  class Generator < Jekyll::Generator
    def generate(site)
      @site    ||= site
      @locales = Jekyll.configuration['locales']
      @pages   = []
      list_pages_with_locales

      @pages.each do |page|
        associate_translations page
      end
    end

    def list_pages_with_locales
      # Pages outside of collections
      @site.pages.each do |page|
        next unless PAGE_EXTENSIONS.include? page.ext
        add_page_with_locale page
      end

      # Pages in collections
      @site.collections.each_pair do |name, collection|
        collection_locale = name.split('_').last
        # Localized collections
        if @locales.keys.include? collection_locale
          collection.docs.each do |page|
            add_page_from_locale_collection page
          end
          # Other collections: treat as page outside of collection
        else
          collection.docs.each do |page|
            add_page_with_locale page
          end
        end
      end
    end

    def add_page_from_locale_collection(page)
      page.data['locale'] = page.collection.label.split('_').last
      @pages << page
    end

    def add_page_with_locale(page)
      raise "Locale not found in frontmatter (#{page.path})" unless page.data['locale']
      raise "Locale identifier not found in frontmatter (#{page.path})" unless page.data['i18n_id']

      @pages << page
    end

    def associate_translations(reference_page)
      reference_locale = reference_page.data['locale']

      # Search for i18n_id in other locales
      @pages.each do |page|
        next if page.data['locale'] == reference_locale || page.data['i18n_id'] != reference_page.data['i18n_id']

        reference_page.data['i18n_links']                      ||= {}
        reference_page.data['i18n_links'][page.data['locale']] = page
        page.data['i18n_links']                                ||= {}
        page.data['i18n_links'][reference_locale]              = reference_page

        break
      end
    end
  end
end

Liquid::Template.register_filter(Translations)

