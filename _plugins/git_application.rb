require 'net/http'

module GitApplication
  def repo_file_link(path)
    "[#{path}](#{@context['site']['application_repo']}/-/blob/master/#{path})"
  end

  class ApplicationHistoryGenerator < Jekyll::Generator
    def generate(site)
      @configuration = Jekyll.configuration
      uri      = URI("#{@configuration['application_api_endpoint']}/tags")
      response = Net::HTTP.get uri
      tags = JSON.parse response

      site.data['last_git_tag'] = tags.first['name']
    end
  end
end

Liquid::Template.register_filter(GitApplication)
