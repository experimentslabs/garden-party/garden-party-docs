require 'byebug'

module Layout
  def outdated(version, text = nil)
    current_tag = @context['site']['documentation_target_tag']
    text        ||= t('layout.outdated')

    icon = current_tag == version ? '✓' : '⚠'
    color = current_tag == version ? 'green' : 'red'
    info = current_tag == version ? t('layout.made_with_current_version') : t('layout.made_with_outdated_version')
    "<span class='label label-#{color} label--expandable'>#{icon} v#{version}<span class='label__info'>#{info}</span></span> <em>#{text}</em>"
  end

  def red_dot(reference)
    "<span class='red-dot'>#{reference}</span>"
  end

  def screenshot(image, version, title = nil)
    raise "Missing screenshot #{image}" unless File.exist? File.join('assets', 'images', image)
    <<~HTML.gsub("\n", '')
      <div class="screenshot">
        <img src="/assets/images/#{image}" alt="#{title}">
        #{outdated version, title}
      </div>
    HTML
  end

  def thumb_section(title, images, *content)
    images = images.split(',').map { |i| "<img src=\"assets/#{i.strip}\" alt=\"#{title}\">" }.join

    <<~HTML.gsub("\n", '')
      <section>
        <h3>#{title}</h3>
        #{images}
        #{content.map { |p| "<p>#{p}</p>" }.join}
      </section>
    HTML
  end
end

Liquid::Template.register_filter(Layout)
