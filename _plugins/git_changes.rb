require 'byebug'

module GitChanges
  # FIXME: Duplicate in translations.rb
  PAGE_EXTENSIONS = ['.html', '.htm', '.markdown', '.md', '.mdown', '.erb'].freeze

  def edit_page_link(article)
    "#{@context['site']['gitlab_repo']}/-/edit/#{@context['site']['gitlab_branch']}/#{article['path']}"
  end

  class Generator < Jekyll::Generator
    def generate(site)
      @site ||= site
      pages.each do |page|
        page.data['modifiable'] = true unless page.data['modifiable'] == false
        output = `git log -1 --format="%ai" -- #{page['path']} |tee`
        commit_date = output.split(' ').first
        page.data['last_modified_date'] = commit_date
      end
    end

    def pages
      return @pages if @pages

      @pages = []

      # Pages outside of collections
      @site.pages.each do |page|
        next unless PAGE_EXTENSIONS.include? page.data['ext']

        @pages << page
      end

      # Pages in collections
      @site.collections.each_value do |collection|
        collection.docs.each do |page|
          next unless PAGE_EXTENSIONS.include? page.data['ext']

          @pages << page
        end
      end

      @pages
    end
  end
end

Liquid::Template.register_filter(GitChanges)
