require 'base64'

# Shamelessly copied from https://gist.github.com/patrickfav/3f9127e25dd6538f0d682b89cbfaefd9

class ContactEmail < Liquid::Tag
  def initialize(tag_name, title, tokens)
    super
    @title = title == '' ? nil : title
  end

  def render(context)
    base64_mail = Base64.strict_encode64 context['site']['email']

    @title = if !@title
               "<script type='text/javascript'>document.write(atob('#{base64_mail}'))</script>"
             else
               @title.sub(/^['"](.*)['"]\s?$/, '\1')
             end
    # See http://techblog.tilllate.com/2008/07/20/ten-methods-to-obfuscate-e-mail-addresses-compared/
    <<~HTML
      <a href="#" data-contact="#{base64_mail}" target="_blank" onfocus="this.href = 'mailto:' + atob(this.dataset.contact)">
        #{@title}
      </a>
    HTML
  end
end

Liquid::Template.register_tag('contact_email_link', ContactEmail)
