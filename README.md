# Garden Party - Documentation

> [Static site](http://doc.garden-party.experimentslabs.com/) used to
> document usage of
> [Garden Party](https://garden-party.experimentslabs.com).

## Development

This uses Jekyll to generate the static site, so you need a working Ruby
setup if you want to run it locally.

```shell
bundle install

# During development
bundle exec jekyll serve

# Build for production
bundle exec jekyll build
```

## Theme, layout, etc...

This documentation uses
"[just-the-docs](https://pmarsceill.github.io/just-the-docs/)" template;
it's better if you directly check its documentation.

## Translations

The translation system is homemade; I wasn't able to find a gem that
works with the theme (just-the-docs).

So, for translations, we have two things: the documentation and the
other pages. Documentation are, in the point of view of Jekyll,
collections. These collections are at the root of the project with the
locale as name (e.g.: `_en/`). The other pages are all the ones _not_ in
a collection (e.g.: root index, 404).

### Translating documentation

To have a match between documents in many languages, an `i18n_id` should
be set in _frontmatter_. It MUST be the same on every translation of the
same page. The locale is then inferred by the name of the collection
during _generation_ time.

```md
# _en/some_topic/some_page.md
---
title: Some page
i18n_id: some_identifier/that/represent_the_page
---

Page content
```

```md
# Translation of the page above
# _fr/un_sujet/une_page.md
---
title: Une page
# Same ID
i18n_id: some_identifier/that/represent_the_page
---

Contenu
```

### Translating other pages

As other pages don't have the convenience of using the collection name
as locale, it must be specified in _frontmatter_:

```md
# /some_page.md
---
title: Some page
i18n_id: something_unique_for_this
locale: en
---

Page content
```

## Contributing

If you ever contribute to Garden Party, you should also change the
documentation accordingly... All merge requests are welcome.

If you want to translate the docs in another language, you're welcome
too! (There is especially a lack of time on our side to make screenshots
of the app in every languages...)

If you're not fond of translating everything, we can help, just ask in
the merge request.

### Tools

#### Screenshots

Screenshots are semi-automatized with `cucumber --profile=documentation`
in Garden Party project. This command generates PNG screenshots in
`tmp/capybara_screenshot`, for the locale specified in
`config/garden_party_instance.yml`. A helper tools is available to
generate them in all languages: `./take-screenshots
<path_to_garden_party>`. Generated files will be in `assets/new_images`,
and needs manual diff/replace. Please, only replace updated ones.

There is no automation for gifs, which means they should be done
manually (we make them with [peek](https://github.com/phw/peek).

#### SVGs

SVGs are minified and cleaned using [SVGO](https://github.com/svg/svgo).
An online service as [SVGOMG!](https://jakearchibald.github.io/svgomg/)
still does the trick if you don't want to install SVGO

We don't have an online shared space to save the unminified assets, but
if you need them, just ask in an issue, we'll find a way to share them.
